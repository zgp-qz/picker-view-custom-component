# 微信小程序 picker-view 自定义组件

`ios 升级 14 之后，微信小程序自带的 picker 组件样式出了问题，官方说正在解决，但是他们解决个 bug 是按照月来数的，毕竟家大业大，总不能动不动更新一次动不动更新一次吧，于是逼着自己写了个相对来说能通用一点的 picker 组件`

## 目录结构

1. components\cusPicker -------- 自定义组件
2. index ----------- 使用例子
3. schema ----------- 例子里边使用 js 生成的数据存放处
4. app.json ----------- 将组件定义成了全局组件
5. ...其他

## 详情

### 通用属性
```
    mode：选择器类型，默认 selector 值：selector - 普通选择器 multiSelector - 多列， cascadeSelector - 级联， time - 时间， date - 日期 
    cusPickerFlag：控制显隐 默认 false - 隐藏
    noText：取消按钮文字，默认 取消
    noTextColor：取消按钮文字颜色 默认 #333333
    noTextBgColor：取消按钮背景颜色 默认 #f5f5f5
    okText：确认按钮文字 默认 确认
    okTextColor：确认按钮文字颜色 默认 #FFFFFF
    okTextBgColor：确认按钮背景颜色 默认 #ff8833
```

### 通用事件
```
    bind:no
    描述：取消事件，这里自行控制关闭组件，在组件内部没做，例子里边有
    返回值：无

    bind:ok
    描述：确定事件
    返回值：Object
    obj.els: 当前整个选中的元素对象，传递的东西没有做任何处理，直接返回，需要什么自己拿
    obj.pickerValue: 当前组件选中的数据对应展示的数据列表的下标，如果需要，自行拿走不谢

```

### 类型介绍

1. selector 普通选择器
    ```
        list: 选项数组
        类型：Array
        例子：
        [
            {
                label:'***',
                other:...
            },
            {
                label:'***',
                other:...
            },
            ...
        ]

        defaultValue: 默认值 
        类型：Array
        例子：[ 0, 0, ...]，数组默认值对应的下标，不是 ID 等标志
    ```
    

2. multiSelector 多列选择器
    ```
        list: 选项数组
        类型：Array
        例子：
        [
            [
                {
                    label:'***',
                    other:...
                },
                {
                    label:'***',
                    other:...
                },
                ...
            ],
            [
                {
                    label:'***',
                    other:...
                },
                {
                    label:'***',
                    other:...
                },
                ...
            ],
            [
                {
                    label:'***',
                    other:...
                },
                {
                    label:'***',
                    other:...
                },
                ...
            ]
        ]
        

        defaultValue: 默认值 
        类型：Array
        例子：[ 0, 0, ...]，数组默认值对应的下标，不是 ID 等标志
    ```


3. cascadeSelector 级联选择器
    `可以是二级、三级联动，现在最多三级联动，因为本地模拟的四级联动的数据的时候，微信小程序死活展示不出来第四级，可能也是性能上的原因或者其他原因吧，暂时适合二三级联动`
    ```
        list: 选项数组
        类型：Array
        例子：
        [
            {
                label:'***',
                other:...,
                children:[
                    {
                        label:'***',
                        other:...,
                        children:[
                            {
                                label:'***',
                                other:...,
                                children:[

                                ]
                            },
                            {
                                label:'***',
                                other:...,
                                children:[
                                    
                                ]
                            },
                            ...
                        ]
                    },
                    {
                        label:'***',
                        other:...,
                        children:[
                            {
                                label:'***',
                                other:...,
                                children:[

                                ]
                            },
                            {
                                label:'***',
                                other:...,
                                children:[
                                    
                                ]
                            },
                            ...
                        ]
                    },
                    ...
                ]
            },
            {
                label:'***',
                other:...,
                children:[
                    {
                        label:'***',
                        other:...,
                        children:[
                            {
                                label:'***',
                                other:...,
                                children:[

                                ]
                            },
                            {
                                label:'***',
                                other:...,
                                children:[
                                    
                                ]
                            },
                            ...
                        ]
                    },
                    {
                        label:'***',
                        other:...,
                        children:[
                            {
                                label:'***',
                                other:...,
                                children:[

                                ]
                            },
                            {
                                label:'***',
                                other:...,
                                children:[
                                    
                                ]
                            },
                            ...
                        ]
                    },
                    ...
                ]
            },
            ...
        ]

        defaultValue: 默认值 
        类型：Array
        例子：[ 0, 0, ...]，数组默认值对应的下标，不是 ID 等标志， 0 对应的元素是 父级 list 对应的下标，1 对应的元素是 父级的子级 children 对应的下标，2 对应的元素是 父级的子级(children)的子级(children) 对应的下标
        点击确定的时候会返回给你
    ```

4. date 日期选择器
    ```
        nowFlag: Boolean 控制是不是只展示到当前日期，默认 true
        fields: String 粒度，默认 day ；day - 年月日，month - 年月 ， year - 年
        startTime: Array 开始时间，默认 [] 例如： [1992,2,24] [年，月，日]
        endTime: Array 开始时间，默认 [] 例如： [2028,2,24] [年，月，日] nowFlag 为 true 的时候 endTime 不起作用
        yearBefore: Number 控制展示当前年份往前数多少年 默认 60 startTime 有值它就不起作用
        yearAfter: Number 控制展示当前年份往后数多少年  默认 60，如果 nowFlag 为 true， yearAfter 将不起作用 endTime 有值它就不起作用
        defaultValue：默认值 数组 Array ,比如：[2019,9,12] 对应 [年 月 日]

        bind:ok
        描述：确定事件
        返回值：Object
        obj.els: 对象，els.year,els.month,els.day,
        obj.pickerValue: 当前组件选中的数据对应展示的数据列表的下标，在年份里边这个无用
    ```

5. time 时间选择器
    ```
        step: 用来控制 分钟 间隔，默认间隔 1 分钟
        maxHour: 用来控制最大小时 默认 24
        minHour: 用来控制最小小时 默认 0
        defaultValue：默认值 数组 Array ,比如：[18,30] 对应 [时，分]

        bind:ok
        描述：确定事件
        返回值：Object
        obj.els: 对象，els.hour,els.minute
        obj.pickerValue: 当前组件选中的数据对应展示的数据列表的下标，在时间里边这个无用
    ```

### 使用方法

见微信小程序文档 [自定义组件](https://developers.weixin.qq.com/miniprogram/dev/framework/custom-component/)