import {
  selectorList,
  selectorData,
  cusMultiList,
  cusMultiData,
  cusCascadeList,
  cusCascadeData
} from '../schema/data'
Page({
  data: {
    modeSelector:'selector',
    modeMultiSelector:'multiSelector',
    modeCascadeSelector:'cascadeSelector',
    modeDate:'date',
    modeTime:'time',

    flagSelector: false,
    flagMultiSelector: false,
    flagCascadeSelector: false,
    flagNowDate: false,
    flagAfterDate: false,
    flagStartEndDate: false,
    flagFieldsDate: false,
    flagTime: false,
    
    selectorList: selectorList,
    selectorData:selectorData,

    multiSelectorList: cusMultiList,
    multiSelectorData:cusMultiData,
    
    cascadeSelectorList: cusCascadeList,
    cascadeSelectorData:cusCascadeData,
    
    fields:'month',
    fieldsNowFlag:true,
    fieldsStart:[1992,9],
    fieldsEnd:[2028,12],
    fieldsDateData:[1996,7],

    dateData:[1998,2,1],
    startTime:[1992,6,8],
    endTime:[2028,6,7],
    
    timeData:[9,6],
    
  },
  okCusPicker: function (e) {
    let val = e.detail;
    wx.showToast({
      icon:'none',
      title: '自行查看控制台返回的数据结构',
      duration:3000
    })
    console.log('点击确定返回的数据结构：',val)
    this.noCusPicker()
  },
  noCusPicker: function () {
    this.setData({
      flagSelector: false,
      flagMultiSelector: false,
      flagCascadeSelector: false,
      flagNowDate: false,
      flagAfterDate: false,
      flagStartEndDate: false,
      flagFieldsDate: false,
      flagTime: false
    })
  },
  showSelector: function (e) {
    let flag = e.currentTarget.dataset.flag;
    this.setData({
      [flag]: true
    })
  },
})